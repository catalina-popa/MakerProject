package ro.devacademy.makerproject.models;

import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by cata on 13.03.2017.
 */

public class Comment implements Parent<CommentReplay> {

    private String title; // make of user and date
    private String text;
    private String numberOFReplies;

    private List<CommentReplay> commentReplays;

    public Comment(String title, String text, String numberOFReplies, List<CommentReplay> commentReplays) {
        this.title = title;
        this.text = text;
        this.numberOFReplies = numberOFReplies;
        this.commentReplays = commentReplays;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNumberOFReplies() {
        return numberOFReplies;
    }

    public void setNumberOFReplies(String numberOFReplies) {
        this.numberOFReplies = numberOFReplies;
    }

    @Override
    public List<CommentReplay> getChildList() {
        return commentReplays;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", numberOFReplies='" + numberOFReplies + '\'' +
                ", commentReplays=" + commentReplays +
                '}';
    }
}
