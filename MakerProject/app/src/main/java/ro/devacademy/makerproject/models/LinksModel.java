package ro.devacademy.makerproject.models;

import java.util.ArrayList;
import java.util.List;

import ro.devacademy.makerproject.server.ApiAdapter;

/**
 * Created by cata on 07.03.2017.
 */

public class LinksModel {


    public void getLinksList (LinksModel.LinksCallback linksCallback){
        //if data in DB -> return from DB
        //else -> network
        ApiAdapter.getInstance().getLinkList(linksCallback);
    }

    public void addNewLink(Link link){
        ApiAdapter.getInstance().addNewLink(link);
    }

    public interface LinksCallback {
        void onLinksReceived(ArrayList<Link> linksList);
    }
}
