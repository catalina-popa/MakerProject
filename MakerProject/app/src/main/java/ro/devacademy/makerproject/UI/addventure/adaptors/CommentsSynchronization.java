package ro.devacademy.makerproject.UI.addventure.adaptors;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import ro.devacademy.makerproject.UI.addventure.RequirementsDetails;

/**
 * Created by cata on 05.04.2017.
 */

public class CommentsSynchronization extends AsyncTask<String, Void, Void> {
    private String TAG = CommentsSynchronization.class.getName();
    private ProgressDialog progressDialog;

    public CommentsSynchronization(RequirementsDetails activity) {
        this.progressDialog = new ProgressDialog(activity);
    }

    @Override
    protected Void doInBackground(String... strings) {
        Log.i(TAG, "doInBackground: " + strings[0]);
        // connect database
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Save into database, please wait.");
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        Log.i(TAG, "onPostExecute: ");
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();

        }
    }
}
