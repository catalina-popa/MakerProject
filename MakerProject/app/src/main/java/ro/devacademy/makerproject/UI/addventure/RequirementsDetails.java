package ro.devacademy.makerproject.UI.addventure;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ro.devacademy.makerproject.Constants;
import ro.devacademy.makerproject.R;
import ro.devacademy.makerproject.UI.addventure.fragments.AddNewLinkFragment;
import ro.devacademy.makerproject.UI.addventure.fragments.CommentsFragment;
import ro.devacademy.makerproject.UI.addventure.fragments.ImagesFragment;
import ro.devacademy.makerproject.UI.addventure.fragments.LinksFragment;
import ro.devacademy.makerproject.UI.addventure.fragments.PublicUrlDialogFragment;
import ro.devacademy.makerproject.UI.addventure.fragments.SendFragment;
import ro.devacademy.makerproject.models.Comment;
import ro.devacademy.makerproject.models.CommentReplay;
import ro.devacademy.makerproject.models.Link;
import ro.devacademy.makerproject.models.LinksModel;

public class RequirementsDetails extends AppCompatActivity implements LinksFragmentsListener {
    private static final String TAG = RequirementsDetails.class.getSimpleName();
    private EditText requirementDetails;
    private TextView requirementData;

    private FrameLayout frameImage;
    private FrameLayout frameLinks;
    private FrameLayout frameSend;
    private FrameLayout frameComments;

    private FragmentManager manager;

    private ArrayList<Link> linkList;

    private Button editRequirementBtn, showPublicUrl;

    private MyListener buttonsOnClickListener = new MyListener();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requirements_details);

        manager = getFragmentManager();

        // get data from server

        // hardcode something
        String text = "As a user when I want to click on login button I should see tweeter login ";

        requirementDetails = (EditText) findViewById(R.id.requirementText);
        requirementDetails.setText(text);

        requirementData = (TextView) findViewById(R.id.requirementDate);
        requirementData.setText("23 Mar 2017");

        editRequirementBtn = (Button) findViewById(R.id.editBtn);
        editRequirementBtn.setOnClickListener(buttonsOnClickListener);

        showPublicUrl = (Button) findViewById(R.id.publicURLBtn);
        showPublicUrl.setOnClickListener(buttonsOnClickListener);

        linkList = new ArrayList<>();


//        Spannable spannable = (Spannable)requirementDetails.getText();
//
//        StyleSpan color = new StyleSpan(Color.BLUE);
//        spannable.setSpan(color, 0, 10, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);


        frameImage = (FrameLayout) findViewById(R.id.container_images);
        frameImage.setVisibility(View.GONE);

        frameLinks = (FrameLayout) findViewById(R.id.container_links);
        frameLinks.setVisibility(View.GONE);

        frameSend = (FrameLayout) findViewById(R.id.container_send);
        frameSend.setVisibility(View.GONE);

        frameComments = (FrameLayout) findViewById(R.id.container_comments);
        frameComments.setVisibility(View.GONE);

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }

    }

    public void showImageFragment(View view) {

        if (frameImage.isShown()) {
            frameImage.setVisibility(View.GONE);
        } else {
            frameImage.setVisibility(View.VISIBLE);

            ImagesFragment fragment = new ImagesFragment();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.container_images, fragment, Constants.ADD_FRAGMENT_IMAGES);
            transaction.commit();
        }
    }

    public void showLinksFragment(View view) {
        if (frameLinks.isShown()) {
            frameLinks.setVisibility(View.GONE);

            // remove add new link if is shown
            AddNewLinkFragment addNewLinkFragment = (AddNewLinkFragment) manager.findFragmentByTag(Constants.ADD_NEW_LINK_FRAGMENT);

            if (addNewLinkFragment != null) {
                removeFragment(addNewLinkFragment);
            }

            // remove links fragment
            LinksFragment linksFragment = (LinksFragment) manager.findFragmentByTag(Constants.ADD_FRAGMENT_LINKS);
            if (linksFragment != null) {
                removeFragment(linksFragment);
            }

        } else {
            frameLinks.setVisibility(View.VISIBLE);

            LinksFragment linksFragment = new LinksFragment();

            // update list from mock
            receiveDataFromMock();
            linksFragment.setData(linkList);

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.container_links, linksFragment, Constants.ADD_FRAGMENT_LINKS);
            transaction.commit();
        }
    }

    public void showSendFragment(View view) {
        if (frameSend.isShown()) {
            frameSend.setVisibility(View.GONE);

            //remove fragment form screen
            SendFragment sendFragment = (SendFragment) manager.findFragmentByTag(Constants.ADD_FRAGMENT_SEND);
            if (sendFragment != null) {
                removeFragment(sendFragment);
            }

        } else {
            frameSend.setVisibility(View.VISIBLE);

            SendFragment sendFragment = new SendFragment();
            FragmentTransaction transaction = manager.beginTransaction();

            transaction.add(R.id.container_send, sendFragment, Constants.ADD_FRAGMENT_SEND);
            transaction.commit();
        }
    }

    @Override
    public void showAddNewLinkFragment() {
        boolean hideFragmentLinks = false;

        // remove links fragment
        LinksFragment linksFragment = (LinksFragment) manager.findFragmentByTag(Constants.ADD_FRAGMENT_LINKS);
        if (linksFragment != null) {
            hideFragmentLinks = true;

            removeFragment(linksFragment);
        }

        if (hideFragmentLinks) {
            FragmentTransaction transaction = manager.beginTransaction();
            AddNewLinkFragment addNewLinkFragment = new AddNewLinkFragment();

            // add new link fragment
            transaction.add(R.id.container_links, addNewLinkFragment, Constants.ADD_NEW_LINK_FRAGMENT);
            transaction.commit();
        }
    }

    @Override
    public void addNewLink(String name, String url) {

        //hide new link fragment
        AddNewLinkFragment newLinkFragment = (AddNewLinkFragment) manager.findFragmentByTag(Constants.ADD_NEW_LINK_FRAGMENT);
        FragmentTransaction transaction = manager.beginTransaction();

        transaction.remove(newLinkFragment);
        transaction.commit();

        updateMockLink(new Link(name, url));

        FragmentTransaction transactionNew = manager.beginTransaction();

        // show LinksFragment
        LinksFragment linksFragment = new LinksFragment();

        // get list from server
        linksFragment.setData(linkList);

        transactionNew.add(R.id.container_links, linksFragment, Constants.ADD_FRAGMENT_LINKS);
        transactionNew.commit();
    }

    public void receiveDataFromMock() {
        LinksModel callback = new LinksModel();
        callback.getLinksList(new LinksModel.LinksCallback() {
            @Override
            public void onLinksReceived(ArrayList<Link> linksList) {
                linkList = linksList;
            }
        });
    }

    public void updateMockLink(Link link) {
        LinksModel linksModel = new LinksModel();
        linksModel.addNewLink(link);
    }

    public void showCommentsFragment(View view) {
        if (frameComments.isShown()) {
            frameComments.setVisibility(View.GONE);

            // delete fragment from screen
            CommentsFragment commentsFragment = (CommentsFragment) manager.findFragmentByTag(Constants.ADD_FRAGMENT_COMMENTS);

            if (commentsFragment != null) {
                removeFragment(commentsFragment);
            }

        } else {
            frameComments.setVisibility(View.VISIBLE);

            CommentsFragment commentsFragment = new CommentsFragment();
            commentsFragment.setData(getSomeComments());

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.container_comments, commentsFragment, Constants.ADD_FRAGMENT_COMMENTS);
            transaction.commit();
        }
    }

    public void removeFragment(Fragment fragment) {
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }

    public ArrayList<Comment> getSomeComments() {
        ArrayList<Comment> comments = new ArrayList<>();
        for (int i = 0; i < 5; i ++ ){
            List<CommentReplay> replays = new ArrayList<>(3);
            replays.add(new CommentReplay("Catalina",  "afara e cerul frumos"));
            replays.add(new CommentReplay("Ana",  "vacanta "));
            replays.add(new CommentReplay("Razvan",  "fain"));

            comments.add(new Comment("Just one comment_ " + i, "neah" ,"3 replyes", replays));
        }

        return comments;
    }

    public class MyListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.editBtn:

                    Log.i(TAG, "onClick: get textview focussable " + editRequirementBtn.isFocusable());

                    editRequirementBtn.setFocusable(true);
                    // if it is on save mode
                    if (editRequirementBtn.getText().toString().contains("save")) {

                        // update text
                        String text = requirementDetails.getText().toString();
                        requirementDetails.setText(text);

                        // disable editing
                        requirementDetails.setEnabled(false);
                        editRequirementBtn.setText(R.string.editTextViewBtn);
                    } else {

                        requirementDetails.setEnabled(true);
                        editRequirementBtn.setText(R.string.saveModifications);
                    }

                    break;

                case R.id.publicURLBtn:
                    Log.i(TAG, "onClick: on the public url button");

                    // create and start dialog box
                    DialogFragment newDialogFragment = new PublicUrlDialogFragment();

                    String linkUrl = "http://catalinaMadeIt.com"; // apel de functie de la server
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.DIALOG_PUBLIC_URL, linkUrl);

                    newDialogFragment.setArguments(bundle);
                    newDialogFragment.show(manager, "Public URL");

                    break;
            }
        }
    }
}
