package ro.devacademy.makerproject.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by cata on 29.03.2017.
 */

public class CommentReplay {

    private String commentName;
    private String commentText;

    public CommentReplay(String commentName, String commentText) {
        this.commentName = commentName;
        this.commentText = commentText;
    }

    public String getCommentName() {
        return commentName;
    }

    public void setCommentName(String commentName) {
        this.commentName = commentName;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }
}
