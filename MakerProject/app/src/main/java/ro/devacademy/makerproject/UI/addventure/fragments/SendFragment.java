package ro.devacademy.makerproject.UI.addventure.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ro.devacademy.makerproject.R;

/**
 * Created by cata on 07.03.2017.
 */

public class SendFragment extends Fragment {

    private Button sendBtn;
    private EditText emailText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_send, container, false);

        // get email address
        emailText = (EditText) view.findViewById(R.id.emailText);

        sendBtn = (Button) view.findViewById(R.id.sendEmail);
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity().getBaseContext(), "Not implemented yet", Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

}
