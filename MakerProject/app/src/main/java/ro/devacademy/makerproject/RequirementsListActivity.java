package ro.devacademy.makerproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.LoginFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import org.w3c.dom.Text;

import java.util.ArrayList;

import ro.devacademy.makerproject.UI.addventure.CreateVentureReq;
import ro.devacademy.makerproject.UI.addventure.RequirementsDetails;
import ro.devacademy.makerproject.models.Requirement;
import ro.devacademy.makerproject.models.Venture;

/**
 * Created by radu on 04.03.2017.
 */

public class RequirementsListActivity extends AppCompatActivity {
    private ArrayList<Requirement> requirementList;
    private RecyclerView recyclerView;
    private RequirementAdapter requirementAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requirements_list);

        //TODO: titlul paginii
        Venture venture = (Venture) getIntent().getSerializableExtra("venture");
        Log.i("Venture", "onCreate: " + venture.getName());
        ((TextView) findViewById(R.id.venture_title)).setText(venture.getName());
        requirementList = (ArrayList<Requirement>) venture.getRequirements();
        //TODO: seteaza ce face butonul ala
        findViewById(R.id.new_story_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newStory = new Intent(RequirementsListActivity.this, CreateVentureReq.class);
                startActivityForResult(newStory, 0);
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.requirements_recycler_view);
        requirementAdapter = new RequirementAdapter(requirementList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(requirementAdapter);
        recyclerView.addOnItemTouchListener(new RequirementTouchListener(this, recyclerView, new RequirementTouchListener.ClickListener(){
            @Override
            public void onClick(View view, int position) {
                Requirement requirement = requirementList.get(position);
                //TODO: start activity -> detalii requirement
                Intent intent = new Intent(RequirementsListActivity.this, RequirementsDetails.class);

                intent.putExtra(Constants.AS_A_FIELD, requirement.getAsA());
                intent.putExtra(Constants.WHEN_I_WANT_FIELD, requirement.getWhenIWant());
                intent.putExtra(Constants.I_SHOULD_SEE_FIELD, requirement.getiShouldSee());

                startActivity(intent);

                Log.i("REQUIREMENT CLICKED", "onClick: " + requirement);
            }
        }));


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null) {
            Requirement requirement = new Requirement(requirementList.size(), data.getStringExtra(Constants.AS_A_FIELD),
                    data.getStringExtra(Constants.WHEN_I_WANT_FIELD), data.getStringExtra(Constants.I_SHOULD_SEE_FIELD));

            requirementAdapter.updateList(requirement);
            requirementAdapter.notifyDataSetChanged();
        }
    }
}
