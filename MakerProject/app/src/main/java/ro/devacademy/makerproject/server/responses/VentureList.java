package ro.devacademy.makerproject.server.responses;

import java.util.List;

import ro.devacademy.makerproject.models.Venture;

/**
 * Created by Cata on 12/17/2016.
 */

public class VentureList {
    private List<Venture> ventureList;

    public VentureList(List<Venture> ventureList) {
        this.ventureList = ventureList;
    }

    public List<Venture> getVentureList() {
        return ventureList;
    }

    public void setVentureList(List<Venture> ventureList) {
        this.ventureList = ventureList;
    }
}
