package ro.devacademy.makerproject.UI.addventure;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import ro.devacademy.makerproject.Constants;
import ro.devacademy.makerproject.R;

public class CreateVentureReq extends AppCompatActivity {

    private static final String TAG = CreateVentureReq.class.getSimpleName();
    private EditText asAField, whenIWantField, iShouldSeeField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_requirement);

        asAField = (EditText) findViewById(R.id.asAField);
        whenIWantField = (EditText) findViewById(R.id.whenIWantField);
        iShouldSeeField = (EditText) findViewById(R.id.iShouldSeeField);

    }

    public void createNewRequirement(View view) {
        Intent intent = new Intent(this, RequirementsDetails.class);

        intent.putExtra(Constants.AS_A_FIELD, asAField.getText().toString());
        intent.putExtra(Constants.WHEN_I_WANT_FIELD, whenIWantField.getText().toString());
        intent.putExtra(Constants.I_SHOULD_SEE_FIELD, iShouldSeeField.getText().toString() );

        Log.i(TAG, "createNewRequirement: " + asAField.getText());

        //startActivity(intent);
        setResult(0, intent);
        finish();
    }
}
