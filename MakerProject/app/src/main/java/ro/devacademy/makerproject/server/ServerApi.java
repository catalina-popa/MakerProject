package ro.devacademy.makerproject.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ro.devacademy.makerproject.models.Link;
import ro.devacademy.makerproject.models.Requirement;
import ro.devacademy.makerproject.models.Venture;

/**
 * Created by Cata on 12/17/2016.
 */

public class ServerApi {

    private ArrayList<Link> linkList;
    private static ServerApi instance = null;

    public static ServerApi getInstance(){
        if(instance == null)
            instance = new ServerApi();

        return instance;
    }


    private ServerApi(){
        linkList = new ArrayList<>();
    }

    public List<Venture> getVentures(){

        List<Venture> listVenture = new ArrayList<>();
        listVenture.add(new Venture(0, "Create Alarm Clock", "", "blue", false, null));
        listVenture.add(new Venture(1, "My new app", "dam dam", "red", false, null));
        listVenture.add(new Venture(2, "crazy app", "party people", "green", false, null));

        listVenture.get(0).addRequirement(new Requirement(1, "User", "to wake up", "the phone blowing up"));
        listVenture.get(0).addRequirement(new Requirement(2, "User", "to go to sleep", "the neighbour's dog peeing"));

        listVenture.get(1).addRequirement(new Requirement(1, "Admin", "make a new app", "GOD"));
        listVenture.get(1).addRequirement(new Requirement(2, "Farmer", "to milk the cow", "the damn cow"));
        listVenture.get(1).addRequirement(new Requirement(3, "Realtor", "to sell a house", "the buyers"));

        listVenture.get(2).addRequirement(new Requirement(1, "User", "get crazy", "a psychiatrist"));
        listVenture.get(2).addRequirement(new Requirement(2, "Bored Programmer", "to do something fun", "a counselor who could help me find a new career"));


        return listVenture;
    }

    public Venture getNewVenture(final String ventureName){
        return new Venture((new Random()).nextInt(1000), ventureName);
    }

    public ArrayList<Link> getLinksList(){
        return linkList;
    }

    public void addNewLink(Link link){
        linkList.add(link);
    }

}
