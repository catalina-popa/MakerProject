package ro.devacademy.makerproject.UI.addventure.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import ro.devacademy.makerproject.Constants;
import ro.devacademy.makerproject.R;
import ro.devacademy.makerproject.UI.addventure.LinksFragmentsListener;
import ro.devacademy.makerproject.UI.addventure.adaptors.LinksAdapter;
import ro.devacademy.makerproject.models.Link;

import static android.content.ContentValues.TAG;

public class LinksFragment extends Fragment {
    private static final int SELECT_IMAGE = 100;

    private Button addNewLinkBtn;

    private RecyclerView recyclerView;
    private LinksAdapter adapter;

    private List<Link> linksList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_links, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewLinks);

        adapter = new LinksAdapter(getActivity().getBaseContext(), linksList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);


        addNewLinkBtn = (Button) view.findViewById(R.id.addNewLinksBtn);
        addNewLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: add new link");
                LinksFragmentsListener listener = (LinksFragmentsListener) getActivity();
                listener.showAddNewLinkFragment();

            }
        });

        //populate list from server
        //addDummyData();

        return view;
    }

    private void addDummyData() {

        adapter.notifyDataSetChanged();
    }

    public void setData(ArrayList<Link> linksList){
        this.linksList = linksList;
    }

}
