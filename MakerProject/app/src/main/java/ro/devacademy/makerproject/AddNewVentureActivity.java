package ro.devacademy.makerproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import ro.devacademy.makerproject.models.Venture;
import ro.devacademy.makerproject.server.ApiAdapter;
import ro.devacademy.makerproject.server.ServerApi;

public class AddNewVentureActivity extends AppCompatActivity {



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_venture);
        Button addButton = (Button) findViewById(R.id.add_new_venture_button);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ventureName = ((EditText)findViewById(R.id.title_edit_text)).getText().toString();
                Log.i("ventureName", "onClick: " + ventureName);

                Venture venture = ApiAdapter.getInstance().getNewVenture(ventureName);

                Bundle bundle = new Bundle();
                bundle.putSerializable("NewVenture", venture);

                Intent intent = new Intent();
                intent.putExtras(bundle);

                setResult(0, intent);
                finish();

            }
        });
    }
}
