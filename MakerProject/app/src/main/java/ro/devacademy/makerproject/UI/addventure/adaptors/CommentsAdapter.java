package ro.devacademy.makerproject.UI.addventure.adaptors;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

import ro.devacademy.makerproject.R;
import ro.devacademy.makerproject.UI.addventure.RequirementsDetails;
import ro.devacademy.makerproject.models.Comment;
import ro.devacademy.makerproject.models.CommentReplay;

/**
 * Created by cata on 13.03.2017.
 */

public class CommentsAdapter extends ExpandableRecyclerAdapter<Comment, CommentReplay, CommentsAdapter.CommentsHolder, CommentsAdapter.CommentsReplyHolder> {

    private static final String TAG = CommentsAdapter.class.getName();

    private LayoutInflater inflater;

    private Context context;
    private RequirementsDetails activity;
    List<Comment> something;

    /**
     * Primary constructor. Sets up {@link #mParentList} and {@link #mFlatItemList}.
     * <p>
     * Any changes to {@link #mParentList} should be made on the original instance, and notified via
     * {@link #notifyParentInserted(int)}
     * {@link #notifyParentRemoved(int)}
     * {@link #notifyParentChanged(int)}
     * {@link #notifyParentRangeInserted(int, int)}
     * {@link #notifyChildInserted(int, int)}
     * {@link #notifyChildRemoved(int, int)}
     * {@link #notifyChildChanged(int, int)}
     * methods and not the notify methods of RecyclerView.Adapter.
     *
     * @param parentList List of all parents to be displayed in the RecyclerView that this
     *                   adapter is linked to
     * @param activity
     */
    public CommentsAdapter(@NonNull List<Comment> parentList, Activity activity) {
        super(parentList);
        this.something = parentList;
        this.activity = (RequirementsDetails)activity;
    }


    @NonNull
    @Override
    public CommentsHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View view = LayoutInflater.from(parentViewGroup.getContext()).inflate(R.layout.comments_format, parentViewGroup, false);
        return new CommentsHolder(view);
    }

    @NonNull
    @Override
    public CommentsReplyHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(childViewGroup.getContext()).inflate(R.layout.comments_replay_format, childViewGroup, false);
        return new CommentsReplyHolder(view);
    }

    @Override
    public void onBindParentViewHolder(@NonNull final CommentsHolder parentViewHolder, final int parentPosition, @NonNull final Comment parent) {
        parentViewHolder.commentTitle.setText(parent.getTitle());
        parentViewHolder.commentText.setText(parent.getText());
        parentViewHolder.numberOfReplies.setText(parent.getNumberOFReplies());

        parentViewHolder.reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i(TAG, "onClick: " + parentViewHolder.reply.getText());

                // find another method
                if (parentViewHolder.reply.getText().equals("reply")) {
                    parentViewHolder.newCommentReplay.setVisibility(View.VISIBLE);
                    parentViewHolder.reply.setText(R.string.saveBtn);
                }
                else if (parentViewHolder.reply.getText().equals("save")){
                    parentViewHolder.newCommentReplay.setVisibility(View.GONE);
                    parentViewHolder.reply.setText(R.string.replyBtn);

                    // save new comment
                    String newComment = parentViewHolder.newCommentReplay.getText().toString();
                    Log.i(TAG, "onClick: new comment= " + newComment);

                    CommentsSynchronization sync = new CommentsSynchronization(activity);
                    sync.execute(newComment);

                    parent.setNumberOFReplies("4 replyes");
                    parent.getChildList().add(new CommentReplay("user", parentViewHolder.newCommentReplay.getText().toString()));
                    notifyChildInserted(parentPosition, 0);
                    notifyParentChanged(parentPosition);
                }
            }
        });
    }

    @Override
    public void onBindChildViewHolder(@NonNull CommentsReplyHolder childViewHolder, int parentPosition, int childPosition, @NonNull CommentReplay child) {
        childViewHolder.commentTitle.setText(child.getCommentName());
        childViewHolder.commentText.setText(child.getCommentText());
    }

    public static class CommentsHolder extends ParentViewHolder {

        private TextView commentTitle;
        private TextView commentText;
        private TextView numberOfReplies;
        private Button reply;

        private EditText newCommentReplay;

        public CommentsHolder(View itemView) {
            super(itemView);

            this.commentTitle = (TextView) itemView.findViewById(R.id.commentTitle);
            this.commentText = (TextView) itemView.findViewById(R.id.commentText);
            this.numberOfReplies = (TextView) itemView.findViewById(R.id.numberOfReplies);

            this.reply = (Button) itemView.findViewById(R.id.replyToSpecificComment);
            this.newCommentReplay = (EditText) itemView.findViewById(R.id.newCommentReply);
        }
    }

    public static class CommentsReplyHolder extends ChildViewHolder {

        private TextView commentTitle;
        private TextView commentText;

        public CommentsReplyHolder(View itemView) {
            super(itemView);
            this.commentTitle = (TextView) itemView.findViewById(R.id.commentReplayTitle);
            this.commentText = (TextView) itemView.findViewById(R.id.commentReplayText);
        }
    }


}
