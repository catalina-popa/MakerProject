package ro.devacademy.makerproject.UI.addventure.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import ro.devacademy.makerproject.Constants;
import ro.devacademy.makerproject.R;

/**
 * Created by cata on 22.03.2017.
 */

public class PublicUrlDialogFragment extends DialogFragment {

    final static String TAG = PublicUrlDialogFragment.class.getName();
    private String publicUrl = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_public_url, null);

        Bundle bundle = getArguments();
        if (bundle != null) {
            publicUrl = bundle.getString(Constants.DIALOG_PUBLIC_URL);
        }

        TextView text = (TextView) view.findViewById(R.id.dialogLinkUrl);
        text.setText(publicUrl);

        builder.setMessage("Project public URL")
                .setView(view)
                .setPositiveButton(R.string.share, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Log.i(TAG, "onClick: pressed share button");

                                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                shareIntent.setType("text/plain");

                                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Project public URL");
                                shareIntent.putExtra(Intent.EXTRA_TEXT, publicUrl);

                                startActivity(Intent.createChooser(shareIntent, "Share via"));
                            }
                        }
                );

        return builder.create();
    }
}
