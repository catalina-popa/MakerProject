package ro.devacademy.makerproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;

/**
 * Created by liviu on 3/12/2017.
 */

public class AuthManager extends Callback<TwitterSession> {
    private Context context;

    public AuthManager(Context context)
    {
        this.context = context;
    }

    @Override
    public void success(Result<TwitterSession> result) {
        TwitterSession session = result.data;

        SharedPreferences sharedPrefs = this.context.getSharedPreferences(
                AuthManager.PREFERENCE_FILE_NAME,
                this.context.MODE_PRIVATE
        );
        SharedPreferences.Editor editor = sharedPrefs.edit();

        editor.putString(AuthManager.PREFERENCE_USERID_KEY, String.valueOf(session.getUserId()));
        editor.putString(AuthManager.PREFERENCE_AUTH_TOKEN_KEY, session.getAuthToken().toString());
        editor.putString(AuthManager.PREFERENCE_USER_NAME_KEY, session.getUserName());
        //Single toggle for being logged in. Simplifies testing
        editor.putString(AuthManager.PREFERENCE_AUTH_PROVIDER_KEY, AuthManager.AUTH_PROVIDER_TWITTER);
        editor.apply();
    }

    @Override
    public void failure(TwitterException exception) {
        Log.d("TwitterKit", "Login with Twitter failure", exception);
    }

    public Boolean userAlreadyLoggedIn()
    {
        SharedPreferences sharedPrefs = this.context.getSharedPreferences(
                AuthManager.PREFERENCE_FILE_NAME,
                this.context.MODE_PRIVATE
        );

        return sharedPrefs.contains(AuthManager.PREFERENCE_AUTH_TOKEN_KEY)
                && !sharedPrefs.getString(AuthManager.PREFERENCE_AUTH_PROVIDER_KEY, "").isEmpty();
    }

    private final static String PREFERENCE_FILE_NAME = "ro.devacademy.makerproject.persistent_auth_file";
    private final static String PREFERENCE_USERID_KEY = "ro.devacademy.makerproject.persistent_auth_file.userid";
    private final static String PREFERENCE_AUTH_TOKEN_KEY = "ro.devacademy.makerproject.persistent_auth_file.auth_token";
    private final static String PREFERENCE_USER_NAME_KEY = "ro.devacademy.makerproject.persistent_auth_file.username";
    private final static String PREFERENCE_AUTH_PROVIDER_KEY = "ro.devacademy.makerproject.persistent_auth_file.auth_provider";

    private final static String AUTH_PROVIDER_TWITTER = "twitter";
}
