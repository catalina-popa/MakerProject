package ro.devacademy.makerproject.server;

import java.util.ArrayList;

import ro.devacademy.makerproject.models.Link;
import ro.devacademy.makerproject.models.LinksModel;
import ro.devacademy.makerproject.models.Venture;
import ro.devacademy.makerproject.models.VenturesModel;

/**
 * Created by Cata on 12/17/2016.
 */

public class ApiAdapter {
    private static ApiAdapter INSTANCE = null;

    private ServerApi serverApi = ServerApi.getInstance();

    private ApiAdapter(){

    }

    public static ApiAdapter getInstance(){
        if (INSTANCE == null)
            INSTANCE = new ApiAdapter();

        return INSTANCE;
    }

    public void getVentureData(final VenturesModel.VenturesCallback venturesCallback){
      venturesCallback.onVenturesReceived(serverApi.getVentures());
        //RETROFIT
    }

    public Venture getNewVenture(final String ventureName){
        return serverApi.getNewVenture(ventureName);
    }


    public void getLinkList(LinksModel.LinksCallback linksCallback){

        linksCallback.onLinksReceived(serverApi.getLinksList());
    }

    public void addNewLink(Link link){
        serverApi.addNewLink(link);
    }

}
