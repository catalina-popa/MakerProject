package ro.devacademy.makerproject.models;

import android.os.Build;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cata on 12/17/2016.
 */

public class Venture implements Serializable{
    private int id;
    private String name;
    private String description;
    private String color;
    private boolean hasIcon;
    private String iconPath;
    private List<Requirement> requirements;

    public Venture(int id, String name, String description, String color, boolean hasIcon, String iconPath) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.color = color;
        this.hasIcon = hasIcon;
        this.iconPath = iconPath;
        requirements = new ArrayList<>();
    }

    public Venture(int id, String name){
        this(id, name, null);
    }

    public Venture(int id, String name, String description){
        this(id, name, description, null, false, null);
    }

    public List<Requirement> getRequirements() {
        return requirements;
    }

    public void addRequirement(Requirement requirement){
        requirements.add(requirement);
    }

    public boolean isHasIcon() {
        return hasIcon;
    }

    public void setHasIcon(boolean hasIcon) {
        this.hasIcon = hasIcon;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Venture{" +
                "id=" + id +
                ", name='" + name + '\'' +
                //", description='" + description + '\'' +
                //", color='" + color + '\'' +
                //", hasIcon=" + hasIcon +
                //", iconPath='" + iconPath + '\'' +
                '}';
    }
}
