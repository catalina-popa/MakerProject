package ro.devacademy.makerproject.UI.addventure.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ro.devacademy.makerproject.R;
import ro.devacademy.makerproject.UI.addventure.RequirementsDetails;
import ro.devacademy.makerproject.UI.addventure.adaptors.CommentsAdapter;
import ro.devacademy.makerproject.UI.addventure.adaptors.CommentsSynchronization;
import ro.devacademy.makerproject.UI.addventure.adaptors.LinksAdapter;
import ro.devacademy.makerproject.models.Comment;
import ro.devacademy.makerproject.models.CommentReplay;

import static io.fabric.sdk.android.Fabric.TAG;

/**
 * Created by cata on 13.03.2017.
 */

public class CommentsFragment extends Fragment {

    private RecyclerView recyclerView;
    private CommentsAdapter adapter;
    private Button replyGeneralBtn;
    private TextView replyCommentText;

    private List<Comment> comments;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_comments, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewComments);
        replyGeneralBtn = (Button) view.findViewById(R.id.addNewCommentBtn);
        replyCommentText = (TextView) view.findViewById(R.id.newComment);

        adapter = new CommentsAdapter(comments, getActivity());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);


        // set action on button
        replyGeneralBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: on reply, add new comment");
                String comment = replyCommentText.getText().toString();

                CommentsSynchronization sync = new CommentsSynchronization((RequirementsDetails)getActivity());
                sync.execute(comment);

                comments.add(new Comment("Catalina said something", comment, "0 replyes", new ArrayList<CommentReplay>()));
                adapter.notifyParentDataSetChanged(true);

                Log.i(TAG, "onClick: " + comments.toString());
            }
        });

        return view;
    }

    public void setData(ArrayList<Comment> comments){
        this.comments = comments;
    }

}
