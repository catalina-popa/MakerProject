package ro.devacademy.makerproject;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ro.devacademy.makerproject.models.Venture;

/**
 * Created by radu on 17.12.2016.
 */

public class VentureAdapter extends RecyclerView.Adapter<VentureAdapter.VentureViewHolder>{

    private List<Venture> ventureList;

    public VentureAdapter(List<Venture> ventureList) {
        this.ventureList = ventureList;
    }

    @Override
    public VentureViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.venture_row, parent, false);

        return new VentureViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VentureViewHolder holder, int position) {
        Venture venture = ventureList.get(position);

        holder.projectTitle.setText(venture.getName());
    }

    @Override
    public int getItemCount() {
        return ventureList.size();
    }

    public void updateList(Venture venture) {
        ventureList.add(venture);
    }

    class VentureViewHolder extends RecyclerView.ViewHolder {
        TextView projectTitle;
        ImageView projectIcon;

        VentureViewHolder(View itemView) {
            super(itemView);
            projectTitle = (TextView) itemView.findViewById(R.id.project_title);
            projectIcon = (ImageView) itemView.findViewById(R.id.project_icon);
        }
    }

}
