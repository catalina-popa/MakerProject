package ro.devacademy.makerproject.UI.addventure.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ro.devacademy.makerproject.R;
import ro.devacademy.makerproject.UI.addventure.LinksFragmentsListener;
import ro.devacademy.makerproject.UI.addventure.RequirementsDetails;

/**
 * Created by cata on 07.03.2017.
 */

public class AddNewLinkFragment extends Fragment {

    private EditText linkNameText;
    private EditText linkUrlText;

    private Button saveBtn;

    private String nameLink, urlLink;

    private static final String URL_HTTP = "http://";
    private static final String URL_HTTPS = "https://";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_new_link, container, false);

        linkNameText = (EditText) view.findViewById(R.id.linkName);
        linkUrlText = (EditText) view.findViewById(R.id.linkUrl);

        saveBtn = (Button) view.findViewById(R.id.saveLink);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nameLink = linkNameText.getText().toString();
                urlLink = linkUrlText.getText().toString();

                // validate urlLink
                if (validateLink(urlLink)) {

                    if (!nameLink.isEmpty()) {
                        LinksFragmentsListener listener = (RequirementsDetails) getActivity();
                        listener.addNewLink(nameLink, urlLink);
                    }
                    else
                        Toast.makeText(getActivity().getBaseContext(), "name not valid", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getActivity().getBaseContext(), "url not valid", Toast.LENGTH_SHORT).show();
                    linkUrlText.setText(null);
                }
            }
        });

        return view;
    }

    private boolean validateLink(String urlLink) {
        if (urlLink.startsWith(URL_HTTP) || urlLink.startsWith(URL_HTTPS))
            return true;

        return false;
    }
}
