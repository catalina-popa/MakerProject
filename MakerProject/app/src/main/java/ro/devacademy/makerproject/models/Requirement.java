package ro.devacademy.makerproject.models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by radu on 04.03.2017.
 */

public class Requirement implements Serializable {
    private Date date;
    private int id;
    private String asA;
    private String whenIWant;
    private String iShouldSee;

    public Requirement(int id, String asA, String whenIWant, String iShouldSee) {
        this.id = id;
        this.asA = asA;
        this.whenIWant = whenIWant;
        this.iShouldSee = iShouldSee;
        date = new Date();
    }

    public String getDate() {
        return (new SimpleDateFormat("dd/mm/yyyy")).format(date);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAsA() {
        return asA;
    }

    public String getWhenIWant() {
        return whenIWant;
    }

    public String getiShouldSee() {
        return iShouldSee;
    }

    public String getIdAsString() {
        return String.valueOf(id);
    }
}
