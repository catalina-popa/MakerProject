package ro.devacademy.makerproject;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ro.devacademy.makerproject.models.Requirement;

/**
 * Created by radu on 04.03.2017.
 */

public class RequirementAdapter extends RecyclerView.Adapter<RequirementAdapter.RequirementViewHolder> {

    private List<Requirement> requirementList;

    public RequirementAdapter(List<Requirement> requirementList) {
        this.requirementList = requirementList;
    }

    @Override
    public RequirementViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.requirement_row, parent, false);

        return new RequirementViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RequirementViewHolder holder, int position) {
        Requirement requirement = requirementList.get(position);

        holder.idTextView.setText(requirement.getIdAsString());
        holder.dateTextView.setText(requirement.getDate());
        holder.descriptionTextView.setText("As a " + requirement.getAsA() + " when I want " +
                requirement.getWhenIWant() + " I should see " + requirement.getiShouldSee());
    }

    @Override
    public int getItemCount() {
        return requirementList.size();
    }

    public void updateList(Requirement newRequirement) {
        requirementList.add(newRequirement);
    }

    class RequirementViewHolder extends RecyclerView.ViewHolder{
        TextView idTextView;
        TextView dateTextView;
        TextView descriptionTextView;

        public RequirementViewHolder(View itemView) {
            super(itemView);

            idTextView = (TextView) itemView.findViewById(R.id.requirement_id);
            dateTextView = (TextView) itemView.findViewById(R.id.requirement_added_date);
            descriptionTextView = (TextView) itemView.findViewById(R.id.requirement_description);
        }
    }

}
