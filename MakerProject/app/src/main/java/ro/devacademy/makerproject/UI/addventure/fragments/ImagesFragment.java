package ro.devacademy.makerproject.UI.addventure.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ro.devacademy.makerproject.R;
import ro.devacademy.makerproject.UI.addventure.adaptors.ImageUploadAdapter;

import static android.content.ContentValues.TAG;

public class ImagesFragment extends Fragment {
    private static final int SELECT_IMAGE = 100;

    private Button uploadImage;
    private ImageView image;

    private RecyclerView recyclerView;
    private ImageUploadAdapter adapter;

    private List<Bitmap> imagesList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_images, container, false);

        image = (ImageView) view.findViewById(R.id.imageLoaded);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewImagesUploaded);
        adapter = new ImageUploadAdapter(imagesList);

        imagesList = new ArrayList<>();

        //just for test
        Bitmap icon = BitmapFactory.decodeResource(getActivity().getBaseContext().getResources(),
                R.drawable.img);

        imagesList.add(icon);
        adapter = new ImageUploadAdapter(imagesList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 4);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);


        uploadImage = (Button) view.findViewById(R.id.uploadImage);
        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
            }
        });

        return view;
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {


                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        Log.i(TAG, "onActivityResult: load an imagine");

                        imagesList.add(bitmap);
                        adapter.notifyDataSetChanged();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            } else
                if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_SHORT).show();
                }
        }
    }
}
