package ro.devacademy.makerproject;

/**
 * Created by Cata on 2/25/2017.
 */

public class Constants {
    public static final String AS_A_FIELD = "asAField";
    public static final String WHEN_I_WANT_FIELD = "whenIWantField";
    public static final String I_SHOULD_SEE_FIELD = "IShouldSeeField";

    public static final String ADD_FRAGMENT_IMAGES = "addFragmentImages";
    public static final String ADD_FRAGMENT_LINKS = "addFragmentLinks";
    public static final String ADD_NEW_LINK_FRAGMENT = "addNewLinkFragment";
    public static final String ADD_FRAGMENT_SEND = "addFragmentSend";
    public static final String ADD_FRAGMENT_COMMENTS = "addCommentsFragment";

    public static final String DIALOG_PUBLIC_URL = "dialogPublicUrl";


    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    public static final String TWITTER_KEY = "DzVbEjtNzKtriupfKyfcFeOaa";
    public static final String TWITTER_SECRET = "oW9KA8cuQTBSKx1mAMOFi5mbh6qJN3GcEwAVZ42RhcEWQ5noeU";

    public static final String REMOVE_FRAGMENT_COMMENTS = "removeCommentsFragment";
}
