package ro.devacademy.makerproject.UI.addventure.adaptors;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ShareCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ro.devacademy.makerproject.R;
import ro.devacademy.makerproject.models.Link;

import static android.content.ContentValues.TAG;

/**
 * Created by cata on 28.02.2017.
 */

public class LinksAdapter extends RecyclerView.Adapter<LinksAdapter.ImageHolder> {

    private LayoutInflater inflater;

    private List<Link> linksViewList = new ArrayList<>();
    private Context context;

    public static class ImageHolder extends RecyclerView.ViewHolder {
        private TextView linkText;
        private ImageView image;

        public ImageHolder(View itemView) {
            super(itemView);

            this.image = (ImageView) itemView.findViewById(R.id.nextIcon);
            this.linkText = (TextView) itemView.findViewById(R.id.textLink);
        }
    }


    public LinksAdapter(Context context, List<Link> imageViewList) {
        this.linksViewList = imageViewList;
        this.context = context;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.from(parent.getContext()).inflate(R.layout.links_format, parent, false);

        return new ImageHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, final int position) {
        holder.linkText.setText(linksViewList.get(position).getName());
        holder.image.setImageResource(R.drawable.ic_navigate_next_black_24dp);
        
        holder.linkText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: pressed on text");

                startBrowser(linksViewList.get(position).getUrl());
                    }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startBrowser(linksViewList.get(position).getUrl());
            }
        });

    }

    private void startBrowser(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(intent);


    }

    @Override
    public int getItemCount() {
        return linksViewList.size();
    }

}
