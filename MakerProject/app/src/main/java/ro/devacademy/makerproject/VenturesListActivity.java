package ro.devacademy.makerproject;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import ro.devacademy.makerproject.UI.ventureslist.VentureProvider;
import ro.devacademy.makerproject.models.Venture;
import ro.devacademy.makerproject.server.ApiAdapter;

public class VenturesListActivity extends AppCompatActivity {
    private ArrayList<Venture> ventureList = new ArrayList<>();
    private RecyclerView recyclerView;
    private VentureAdapter ventureAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventures_list);

        recyclerView = (RecyclerView) findViewById(R.id.ventures_recycler_view);
        ventureAdapter = new VentureAdapter(ventureList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(ventureAdapter);
        recyclerView.addOnItemTouchListener(new VentureTouchListener(this, recyclerView, new VentureTouchListener.ClickListener(){

            @Override
            public void onClick(View view, int position) {
                Venture venture = ventureList.get(position);
                //TODO: start activity with position
                //TODO: get info from server

                //TODO: wait for response (loading)


                //TODO: send intent

                Intent intent = new Intent(VenturesListActivity.this, RequirementsListActivity.class);
                intent.putExtra("venture", venture);

                startActivity(intent);

                Log.i("onCreate", "onClick: " + venture);
            }
        }));


        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.new_venture_floating_action_button);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addVenture = new Intent(VenturesListActivity.this, AddNewVentureActivity.class);
                //Bundle bundle = new Bundle();
                //bundle.putSerializable("List", ventureList);
                //addVenture.putExtras(bundle);
                startActivityForResult(addVenture, 0); //ca sa fac update la lista
            }
        });

        populate(ventureList);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null) {
            ventureAdapter.updateList((Venture) data.getSerializableExtra("NewVenture"));
            ventureAdapter.notifyDataSetChanged();
        }
        else{
            Log.i("How's data?", "onActivityResult: data is " + data);
        }
    }

    private void populate(List<Venture> ventures){
        VentureProvider ventureProvider = VentureProvider.getInstance(VenturesListActivity.this);

        for(int i = 0; i<ventureProvider.getCount(); i++){
            ventures.add(ventureProvider.getItem(i));
        }

    }
}
