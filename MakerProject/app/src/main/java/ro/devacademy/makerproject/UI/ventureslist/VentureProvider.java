package ro.devacademy.makerproject.UI.ventureslist;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ro.devacademy.makerproject.models.Venture;
import ro.devacademy.makerproject.models.VenturesModel;

/**
 * Created by Cata on 12/17/2016.
 */

public class VentureProvider {

    private static String TAG = VentureProvider.class.toString();
    private Context context;
    private List<Venture> venturesListProvaider;

    public static VentureProvider INSTANCE = null;

    public  static VentureProvider getInstance(Context context){
        if(INSTANCE == null)
            INSTANCE = new VentureProvider(context);

        return INSTANCE;
    }

    private VentureProvider(Context context){
        this.context = context;
        venturesListProvaider = new ArrayList<>();

        extractData();
    }

    private void extractData(){
        VenturesModel venturesModel = new VenturesModel();
        venturesModel.getVentures(new VenturesModel.VenturesCallback() {
            @Override
            public void onVenturesReceived(List<Venture> ventureList) {
                Log.i(TAG, "onVenturesReceived: " + ventureList);
                venturesListProvaider = ventureList;
            }
        });
    }

    public Venture getItem(int position){
        return venturesListProvaider.get(position);
    }

    public int getCount(){
        return venturesListProvaider.size();
    }

    public void addItem(Venture venture){
        venturesListProvaider.add(venture);
    }
}
