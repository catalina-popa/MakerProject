package ro.devacademy.makerproject.UI.addventure;

/**
 * Created by cata on 07.03.2017.
 */

public interface LinksFragmentsListener {

    void showAddNewLinkFragment();

    void addNewLink(String name, String url);

}
